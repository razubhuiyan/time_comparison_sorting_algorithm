#include <iostream>
#include <cstdlib>
#include <ctime>
#include<time.h>

using namespace std;
long length = 1000;
//bubblesort start
void bubbleSort(int *);
void bubbleSort(int *list)
{
    int temp;
    for(long i = 0; i < length; i++)
    {
        for(long j = 0; j < length-i-1; j++)
        {
            if (list[j] > list[j+1])
            {
                temp        = list[j];
                list[j]     = list[j+1];
                list[j+1]   = temp;
            }
        }
    }
}
//bubblesort end 

//Insertionsort start
void insertionSort(int *);
void insertionSort(int *list)
{
    int temp;
    for(long i = 1; i < length; i++)
    {
        temp = list[i];
        long j;
        for(j = i-1; j >= 0 && list[j] > temp; j--)
        {
            list[j+1] = list[j];
        }
        list[j+1] = temp;
    }
}
//Insertionsort end

//mergesort start
void merge(int *,int, int , int );
void mergeSort(int *arr, int low, int high)
{
    int mid;
    if (low < high){
        mid=(low+high)/2;
        mergeSort(arr,low,mid);
        mergeSort(arr,mid+1,high);
        merge(arr,low,high,mid);
    }
}
// Merge sort 
void merge(int *arr, int low, int high, int mid)
{
    int i, j, k, c[100000];
    i = low;
    k = low;
    j = mid + 1;
    while (i <= mid && j <= high) {
        if (arr[i] < arr[j]) {
            c[k] = arr[i];
            k++;
            i++;
        }
        else  {
            c[k] = arr[j];
            k++;
            j++;
        }
    }
    while (i <= mid) {
        c[k] = arr[i];
        k++;
        i++;
    }
    while (j <= high) {
        c[k] = arr[j];
        k++;
        j++;
    }
    for (i = low; i < k; i++)  {
        arr[i] = c[i];
    }
}
//mergesort end

//heapsort start

// function to heapify the tree
void heapify(int *, int, int);
void heapify(int *array, int n, int root)
{
   int largest = root; 
   int l = 2*root + 1;
   int r = 2*root + 2;
  
   if (l < n && array[l] > array[largest])
   largest = l;
  
   if (r < n && array[r] > array[largest])
   largest = r;
  
   if (largest != root)
      {
      swap(array[root], array[largest]);
  
      heapify(array, n, largest);
      }
}
  
// implementing heap sort
void heapSort(int *array, int n)
{
   for (int i = n / 2 - 1; i >= 0; i--)
   heapify(array, n, i);
  
   for (int i=n-1; i>=0; i--)
   {
      swap(array[0], array[i]);
  
      heapify(array, i, 0);
   }
}
//heapsort end

//quicksort start
// A utility function to swap two elements 
void swap(int* a, int* b) 
{ 
    int t = *a; 
    *a = *b; 
    *b = t; 
} 

int partition(int *, int, int);
int partition (int *arr, int low, int high) 
{ 
    int pivot = arr[high]; 
    int i = (low - 1);   
  
    for (int j = low; j <= high- 1; j++) 
    { 
        if (arr[j] <= pivot) 
        { 
            i++;
            swap(&arr[i], &arr[j]); 
        } 
    } 
    swap(&arr[i + 1], &arr[high]); 
    return (i + 1); 
} 

void quickSort(int *, int, int);
void quickSort(int *arr, int low, int high)
{ 
    if (low < high) 
    { 
        int pi = partition(arr, low, high); 
  
        quickSort(arr, low, pi - 1); 
        quickSort(arr, pi + 1, high); 
    } 
} 
//quicksort end

int main()
{
    long length = 1000;
    const long max_length = 10000;
    int array[max_length];
    double t1, t2;
    const double CLK_TCK = 1000.0;
    srand((unsigned)time(0));
    for(int i=0; i<max_length; i++){ 
        array[i] = (rand()%length)+1;
    }
    
    t1 = clock();
    bubbleSort(array);
    t2 = clock();
    cout << "Bubble Sort\t: " << (t2 - t1)/CLK_TCK << " sec\n";
    
    t1 = clock();
    insertionSort(array);
    t2 = clock();
    cout << "Insertion Sort\t: " << (t2 - t1)/CLK_TCK << " sec\n";
    
    t1 = clock();
    mergeSort(array, 0, max_length-1);
    t2 = clock();
    cout << "Merge Sort\t: " << (t2 - t1)/CLK_TCK << " sec\n";
    
    int n = sizeof(array)/sizeof(array[0]);
    t1 = clock();
    heapSort(array, n);
    t2 = clock();
    cout << "Heap Sort\t: " << (t2 - t1)/CLK_TCK << " sec\n";
    
    int m = sizeof(array)/sizeof(array[0]);
    t1 = clock();
    quickSort(array, 0, m-1);
    t2 = clock();
    cout << "Quick Sort\t: " << (t2 - t1)/CLK_TCK << " sec\n";
    
    return 0;
}
